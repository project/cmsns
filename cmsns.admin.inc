<?php

function cmsns_admin_settings() {
	/*$form['hao'] = array(
		'#title' => t('好友设置'),
		'#weight' => -4,
		'#type' => 'fieldset',
		'#collapsible' => TRUE,
	);*/
	$form['qian'] = array(
		'#title' => t('Wallet Settings'),
		'#weight' => -3,
		'#type' => 'fieldset',
		'#collapsible' => TRUE,
	);
	/*$form['xiao'] = array(
		'#title' => t('信件设置'),
		'#weight' => -2,
		'#type' => 'fieldset',
		'#collapsible' => TRUE,
	);*/
	$form['qian']['addnode'] = array(
		'#title' => t('Add Node/Comment Reward Amount'),
		'#type' => 'fieldset',
	);
	$form['qian']['delnode'] = array(
		'#title' => t('Delete Node/Comment Deducted Amount'),
		'#type' => 'fieldset',
	);
	$form['qian']['user'] = array(
		'#title' => t('User Settings'),
		'#type' => 'fieldset',
	);
  $types = node_get_types();
  foreach ($types as $type){
  	$form['qian']['addnode']['cmsns_add_'.$type->type] = array(
  		'#title' => $type->name,
  		'#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 10,
  		'#default_value' => variable_get('cmsns_add_'.$type->type, 5),
  	);
  	$form['qian']['addnode']['cmsns_addcomment_'.$type->type] = array(
  		'#title' => t('Comment for !name', array('!name' => $type->name)),
  		'#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 10,
  		'#default_value' => variable_get('cmsns_addcomment_'.$type->type, 3),
  	);
  	$form['qian']['delnode']['cmsns_del_'.$type->type] = array(
  		'#title' => $type->name,
  		'#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 10,
  		'#default_value' => variable_get('cmsns_del_'.$type->type, 5),
  	);
  	$form['qian']['delnode']['cmsns_delcomment_'.$type->type] = array(
  		'#title' => t('Comment for !name', array('!name' => $type->name)),
  		'#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 10,
  		'#default_value' => variable_get('cmsns_delcomment_'.$type->type, 3),
  	);
  }
	$form['qian']['user']['reg'] = array(
		'#title' => t('Reward of suceessful register'),
		'#type' => 'textfield',
    '#required' => TRUE,
    '#size' => 10,
		'#default_value' => variable_get('cmsns_reg', 100),
	);
	return system_settings_form($form);
}
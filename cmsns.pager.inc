<?php

function cmsns_xiaoxi_page_view($ac){
  if(!$_GET['his']){
    $result = pager_query('SELECT * FROM {x_xiaoxi} WHERE touid = %d OR uid = %d ORDER BY id DESC', 20, 0, null, $ac->uid, $ac->uid);
    drupal_set_title(t('Records of All My Mails'));
  }elseif(is_numeric($_GET['his'])){
    $result = pager_query('SELECT * FROM {x_xiaoxi} WHERE touid IN (%d, %d) OR uid IN (%d, %d) ORDER BY id DESC', 20, 0, null, $ac->uid, $_GET['his'], $_GET['his'], $ac->uid);
    $tu = $u = _cmsns_qianbao_user(array('uid' => $_GET['his']));
    drupal_set_title(t('Records of Mail for !u and I', array('!u' => $tu->name)));
  }
  $i = 0;
  while ($a = db_fetch_object($result)){
    if(!$_GET['his']){
      $u = _cmsns_qianbao_user(array('uid' => $a->touid));
      $tu = _cmsns_qianbao_user(array('uid' => $a->uid));
    }
    if($a->touid == $ac->uid){
      $loc = t('!u sent to me', array('!u' => theme('cmsns_username', $tu)));
      $gourl = "user/$ac->uid/xiaoxi/get/$a->uid";
      if($a->tostatus == 1){
        $s = '<span class="f00">'.t('Unread').'</span>';
      }else{
        $s = t('Already Read');
      }
    }else{
      $loc = t('I Sent to', array('!u' => theme('cmsns_username', $u)));
      $gourl = "user/$ac->uid/xiaoxi/set/$a->uid";
      if($a->tostatus == 1){
        $s = '<span class="f00">'.t('Have not read by recipients').'</span>';
      }else{
        $s = t('Already Read by recipients');
      }
    }
    $rows[] = array($loc, l(check_plain($a->title), $gourl, array('fragment' => "msg-$a->id", 'query' => array('msg' => $a->id))), t('%t ago', array('%t' => format_interval(time() - $a->atime))), $s);
    $i = true;
  }
  if($i){
    $v = theme('table', array(t('To'), t('Subject'), t('Received Time'), t('Mail Status')), $rows, array('id' => 'xiaoxibox'));
    $v .= theme('pager', NULL, 20, 0);
  }else{
    $v = t('No Mails');
  }
  $v .= '<h2>'.t('Write a mail').'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_form');
  return $v;
}

function cmsns_xiaoxi_page_set($ac){
  global $user;
  if(!is_numeric(arg(4))){
    $result = pager_query('SELECT id, touid, tostatus, atime, title FROM {x_xiaoxi} WHERE uid = '.$user->uid . ' AND status = 1 ORDER BY tostatus ASC, atime DESC', 20, 0);
    $true = false;
    while ($a = db_fetch_object($result)){
      $u = _cmsns_qianbao_user(array('uid' => $a->touid));
      if($a->tostatus == 1){
        $s = '<span class="f00">'.t('Have not read by recipients').'</span>';
      }else{
        $s = t('Already Read by recipients');
      }
      $t[$a->id] = array('title' => l(check_plain($a->title), "user/$user->uid/xiaoxi/set/$a->id"), 'id' => $a->id, 'name' => theme('cmsns_username', $u), 'time' => t('% ago', array('%t' => format_interval(time() - $a->atime))), 's' => $s);
      $true = true;
    }
    if($true){
      $v = drupal_get_form('cmsns_xiaoxi_msg_lists', $t, 'set');
      $v .= theme('pager', NULL, 20, 0);
    }else{
      $v = '<h2>'.t('Sent-Box is empty').'</h2>';
    }
    $v .= '<h2>'.t('Write a mail').'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_form');
    drupal_set_title(t('Sent'));
  }else{
    $id = arg(4);
    $result = db_query('SELECT x.*, b.body FROM {x_xiaoxi} x INNER JOIN {x_xiaoxi_body} b ON x.xid = b.xid WHERE x.id = '.$id. ' = 1 AND x.uid = '.$user->uid);
    $true = false;
    while ($a = db_fetch_object($result)){
      if($a->status == 1){
        $ut = _cmsns_qianbao_user(array('uid' => $a->touid));
        $value['name'] = $ut->name;
        if($a->tostatus == 1){
          $s = '<span class="f00">'.t('Have not read by recipients').'</span>';
        }else{
          $s = t('Already Read by recipients');
        }
        $rows[] = '<span class="msg-title">'.t('Subject:').'</span>'.check_plain($a->title);
        $rows[] = '<span class="msg-title">'.t('Send to:').'</span>'.theme('cmsns_username',$ut).' ('.l(t('View All Mails'), "user/$ac->uid/xiaoxi", array('query' => array('his' => $a->touid))).')';
        $rows[] = '<span class="msg-title">'.t('Time:').'</span>'.format_date($a->atime,'small');
        $rows[] = '<span class="msg-title">'.t('Body:').'</span><div class="msg-body">'.check_markup($a->body, 0, FALSE).'</div>';
        drupal_set_title(t('Sent Mail @title', array('@title' => check_plain($a->title))));
      }else{
        return '<h2>'.t('You have deleted this mail.').'</h2>';
      }
      $true = true;
    }
    if($true){
      $v = theme('item_list',$rows);
      $v .= '<h2>'.t('Write a mail to !name again', array('!name' => $ut->name)).'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_form',$value);
    }else{
      $v = '<h2>'.t('Illegal access').'</h2>';
    }
  }
  return $v;
}

function cmsns_xiaoxi_page_get($ac){
  if(is_numeric(arg(4))){
    $a = db_fetch_object(db_query('SELECT x.*, b.body FROM {x_xiaoxi} x INNER JOIN {x_xiaoxi_body} b ON x.xid = b.xid WHERE x.id = %d AND x.touid = %d', arg(4), $ac->uid));
    if($a->uid){
      if($a->tostatus != 0){
        $ut = _cmsns_qianbao_user(array('uid' => $a->uid));
        $value['name'] = $ut->name;
        $rows[] = '<span class="msg-title">'.t('Subject:').'</span>'.check_plain($a->title);
        $rows[] = '<span class="msg-title">'.t('From:').'</span>'.theme('cmsns_username',$ut).' ('.l(t('View All Mails'), "user/$ac->uid/xiaoxi", array('query' => array('his' => $a->uid))).')';
        if($a->tostatus == 1){
          db_query("UPDATE {x_xiaoxi} SET tostatus = 2 WHERE id = %d", $a->id);
        }
        $rows[] = '<span class="msg-title">'.t('Time:').'</span>'.format_date($a->atime,'small');
        $rows[] = '<span class="msg-title">'.t('Body:').'</span><div class="msg-body">'.check_markup($a->body, 0, FALSE).'</div>';
        $value['title'] = t('Re:').$a->title;
        $v = theme('item_list', $rows);
        $v .= '<h2>'.t('Reply to !name',array('!name' => $ut->name)).'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_form',$value);
        drupal_set_title(t('Inbox @title', array('@title' => check_plain($a->title))));
      }else{
       return '<h2>'.t('You have deleted this mail.').'</h2>';
      }
    }else{
      return '<h2>'.t('Illegal access').'</h2>';
    }
  }else{
    $result = pager_query('SELECT uid, id, touid, tostatus, atime, title FROM {x_xiaoxi} WHERE touid = '.$ac->uid . ' AND status = 1 ORDER BY tostatus ASC, atime DESC', 20, 0);
    $true = false;
    while ($a = db_fetch_object($result)){
      $u = _cmsns_qianbao_user(array('uid' => $a->uid));
      if($a->tostatus == 1){
        $s = '<span class="f00">'.t('Unread').'</span>';
      }else{
        $s = t('Already Read');
      }
      $t[$a->id] = array('title' => l(check_plain($a->title), "user/$ac->uid/xiaoxi/get/$a->id"), 'id' => $a->id, 'name' => theme('cmsns_username', $u), 'time' => t('%t ago', array('%t' => format_interval(time() - $a->atime))), 's' => $s);
      $true = true;
    }
    if($true){
      $v = drupal_get_form('cmsns_xiaoxi_msg_lists', $t, 'set');
      $v .= theme('pager', NULL, 20, 0);
    }else{
      $v = '<h2>'.t('Inbox is empty.').'</h2>';
    }
    $v .= '<h2>'.t('Write a mail').'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_form');
    drupal_set_title(t('Inbox'));
  }
  return $v;
}

function cmsns_xiaoxi_page_sys($ac){
  global $user;
  $result = pager_query('SELECT * FROM {x_xiaoxi_body} WHERE status = 2 ORDER BY xid ASC', 50, 0);
  $true = false;
  switch (arg(4)){
    case 'view':
      $i = 1; 
      while ($a = db_fetch_object($result)){
        $u = _cmsns_qianbao_user(array('uid' => $a->uid));
        $rows[] = '<span id="sys-'.$a->xid.'" class="xiaoxititle">'.$i.'</span>'.t('!n posted this message on %t (%t ago):', array('!n' => theme('cmsns_username', $u), '%t' => format_date($a->ctime,'small'), '%i' => format_interval(time() - $a->ctime))).'<div class="system-title"><span class="system-t">'.t('Subject:').'</span>'.$a->title.'</div><div class="system-body"><div class="system-z">'.t('Body:').'</div>'.check_markup($a->body, 0, FALSE).'</div>';
        $i++;
      }
      if($i){
        $v = theme('item_list', $rows);
      }else{
        $v = '<h2>'.t('Illegal access').'</h2>';
      }
    break;
    case 'edit':
      $result = db_query('SELECT * FROM {x_xiaoxi_body} WHERE xid = %d AND  status = 2', arg(5));
      $true = false;
      while ($a = db_fetch_object($result)){
        $c['title'] = $a->title;
        $c['body'] = $a->body;
        $c['xid'] = $a->xid;
        $true = true;
      }
      if($true && user_access('create notice')){
        $c['type'] = 'edit';
        $v .= '<h2>'.t('Edit System Message').'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_sys_form',$c);
      }else{
        $v = '<h2>'.t('Illegal access').'</h2>';
      }
    break;
    default:
      $result = pager_query('SELECT * FROM {x_xiaoxi_body} WHERE status = 2 ORDER BY xid DESC', 50, 0);
      while ($a = db_fetch_object($result)){
        $u = _cmsns_qianbao_user(array('uid' => $a->uid));
        if(!user_access('create notice')){
          $rows[] = array(theme('username', $u), l($a->title,"user/$user->uid/xiaoxi/sys/view", array('fragment' => "sys-$a->xid")), format_interval(time() - $a->ctime));
        }else{
          $t[$a->xid] = array('title' => l($a->title, "user/$user->uid/xiaoxi/sys/view", array('fragment' => "sys-$a->xid")), 'id' => $a->xid, 'name' => theme('username', $u), 'time' => t('%t ago', array('%t' => format_interval(time() - $a->ctime))), 's' => l(t('Edit'),"user/$user->uid/xiaoxi/sys/edit/$a->xid", array('query' => drupal_get_destination())));
        }
        $true = true;
      }
      if($true){
        if($user->uid == 1 || user_access('create notice')){
          $v = drupal_get_form('cmsns_xiaoxi_msg_lists', $t, 'sys');
          $v .= '<h2>'.t('Post System Message').'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_sys_form');
        }else{
          $v = theme('table', array(t('Post by'), t('Subject'), t('Post Time')), $rows, array('id' => 'xiaoxibox'));
        }
        $v .= theme('pager', NULL, 50, 0);
      }else{
        $v = '<h2>'.t('No System Message').'</h2>';
        if($user->uid == 1 || user_access('create notice')){
          $v .= '<h2>'.t('Post System Message').'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_sys_form');
        }
      }
    break;
  }
  drupal_set_title(t('System Message'));
  return $v;
}

function cmsns_xiaoxi_page_edit($ac){
  global $user;
  if(!is_numeric(arg(4))){
    $result = pager_query('SELECT * FROM {x_xiaoxi_body} WHERE uid = '.$user->uid.' AND status = 0 ORDER BY xid DESC', 20, 0);
    $true = false;
    while ($a = db_fetch_object($result)){
      $t[$a->xid] = array('title' => l(check_plain($a->title),"user/$user->uid/xiaoxi/edit/$a->xid", array('query' => drupal_get_destination())), 'id' => $a->xid, 'time' => t('%t之前', array('%t' => format_interval(time() - $a->ctime))), 's' => l(t('Edit'), "user/$user->uid/xiaoxi/edit/$a->xid", array('query' => drupal_get_destination())));
      $true = true;
    }
    if($true){
      $v = drupal_get_form('cmsns_xiaoxi_msg_lists', $t, 'edit');
      $v .= theme('pager', NULL, 20, 0);
    }else{
      $v = '<h2>'.t('Draft box is empty').'</h2>';
    }
    $v .= '<h2>'.t('Write a mail').'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_form');
    drupal_set_title(t('Draft Box'));
  }else{
    $result = db_query('SELECT * FROM {x_xiaoxi_body} WHERE xid = %d AND uid = %d AND status = 0', arg(4), $user->uid);
    $true = false;
    while ($a = db_fetch_object($result)){
      $c['title'] = $a->title;
      $c['body'] = $a->body;
      $c['xid'] = $a->xid;
      $true = true;
    }
    if($true){
      $c['type'] = 'edit';
      $v .= '<h2>'.t('Edit Draft').'</h2>'.drupal_get_form('cmsns_xiaoxi_msg_form',$c);
    }else{
      $v = '<h2>'.t('Illegal access').'</h2>';
    }
    drupal_set_title(t('Edit Draft'));
  }
  return $v;
}

function cmsns_xiaoxi_msg_lists(& $form_state, $value, $type){
  foreach ($value as $key => $x){
    $form['new'][$key]['del'] = array('#type' => 'checkbox');
    $form['new'][$key]['name'] = array('#value' => $x['name']);
    $form['new'][$key]['title'] = array('#value' => $x['title']);
    $form['new'][$key]['time'] = array('#value' => $x['time']);
    $form['new'][$key]['s'] = array('#value' => $x['s']);
    $form['new'][$key]['t'] = array('#type' => 'value', '#value' => $x['title']);
  }
  $form['type'] = array('#type' => 'value', '#value' => $type);
  $form['new']['#tree'] = true;
  $form['new']['#theme'] = 'cmsns_xiaoxi_msg_lists_table';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm to delete selected content'),
    '#submit' => array('cmsns_xiaoxi_msg_lists_submit'),
  );
  return $form;
}

function cmsns_xiaoxi_msg_lists_submit($form, & $form_state) {
	global $user;
	foreach($form_state['values']['new'] as $id => $key) {
		if ($key['del'] == 1) {
      if(cmsns_xiaoxi_msg_del(array('id' => $id, 'type' => $form_state['values']['type']))){
			  drupal_set_message(t('Mail !t has been successfully deleted from you inbox.', array('!t' => $key['t'])));
      }else{
        drupal_set_message(t('Failed to remove !it', array('!t' =>$key['t'])));
      }
		}
	}
}

function theme_cmsns_xiaoxi_msg_lists_table($form) {
  foreach (element_children($form) as $key) {
    $row = array();
    $row[] = drupal_render($form[$key]['del']);
    if(arg(3) != 'edit'){
      $row[] = drupal_render($form[$key]['name']);
    }
    $row[] = drupal_render($form[$key]['title']);
    $row[] = drupal_render($form[$key]['time']);
    $row[] = drupal_render($form[$key]['s']);
    $rows[] = $row;
  }
  switch (arg(3)){
    case 'set':
      $header = array(t('Delete'), t('Send to'),t('Subject'),t('Time'), t('Status'));
    break;
    case 'get':
      $header = array(t('Delete'), t('From'),t('Subject'),t('Time'), t('Status'));
    break;
    case 'edit':
      $header = array(t('Delete'), t('Subject'),t('Created Time'), t('Edit'));
    break;
    case 'sys':
      $header = array(t('Delete'), t('Post by'),t('Subject'),t('Post Time'), t('Edit'));
    break;
  }
  $t = theme('table', $header, $rows, array('id'=>'xiaoxibox'));
  return $t;
}

function cmsns_xiaoxi_msg_sys_form(& $form_state, $v = false){
  global $user;
	$form['title'] = array(
    '#type'=> 'textfield',
    '#title' => t('Subject'),
    '#default_value' => $v['title'],
    '#size'  => 80,
    '#required' => TRUE,
    '#maxlength' => 128,
	);
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => $v['body'],
    '#required' => TRUE,
    '#cols' => 40,
    '#rows' => 10
  );
  
  $form['v'] = array(
  	'#type' => 'value',
  	'#value' => $v,
  );
  
  $form['submit'] = array(
  	'#type' => 'submit',
  	'#value' => t('Confirm to post'),
  	'#submit' => array('cmsns_xiaoxi_msg_sys_form_submit'),
  );
  $form['#validate'][] = 'cmsns_xiaoxi_msg_sys_form_validate';
  return $form;
}

function cmsns_xiaoxi_msg_sys_form_validate($form, & $form_state) {
  global $user;
  if($form_state['values']['title'] == '' && $form_state['values']['body'] == '' ){
    form_set_error('title', t('Body and Title cannot be empty.'));
  }
}

function cmsns_xiaoxi_msg_sys_form_submit($form, & $form_state) {
  global $user;
  $v = $form_state['values']['v'];
  cmsns_xiaoxi_msg_set(array('status' => 2,'title' => $form_state['values']['title'], 'body' => $form_state['values']['body'],  'type' => $v['type'], 'xid' => $v['xid']));
}

function cmsns_page_privacy(& $form_state, $ac){
	$u = cmsns_privacy($ac->uid);
	$form['xiaoxi'] = array(
		'#type' => 'fieldset',
		'#title' => t('Private Option'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#attributes' => array('class' => 'menu-item-form'),
	);
	$form['xiaoxi']['x'] = array(
		'#type' => 'radios',
		'#title' => t('Private Option'),
		'#default_value' => $u->xiaoxi ? $u->xiaoxi: 0,
		'#options' => array(
      t('Only received friends message'), 
      t('Accept to receive message from anybody'),
      t('Do Not receive message'), 
    ),
	);
	$form['haoyou'] = array(
		'#type' => 'fieldset',
		'#title' => t('Friends Settings'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#attributes' => array('class' => 'menu-item-form'),
	);
	$form['haoyou']['h'] = array(
		'#type' => 'radios',
		'#title' => t('Private Option'),
		'#default_value' => $u->haoyou ? $u->haoyou: 0,
		'#options' => array(
     t('Need confirm to be a friend'), 
     t('Confirm friends do not need to verify'),
     t('Not allow to add me be a friend'), 
    ),
	);
	$form['qianbao'] = array(
		'#type' => 'fieldset',
		'#title' => t('Wallet Settings'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
		'#attributes' => array('class' => 'menu-item-form'),
	);
	$form['qianbao']['q'] = array(
		'#type' => 'radios',
		'#title' => t('Private Option'),
		'#default_value' => $u->qianbao ? $u->qianbao: 0,
		'#options' => array(
      t('Not all anybody to view my account'),
      t('Allow friend to view my account'),
      t('Allow anybody to view my account'),
    ),
    '#disabled' => true
	);
  $form['v'] = array(
  	'#type' => 'value',
  	'#value' => $u->uid,
  );
  
  $form['submit'] = array(
  	'#type' => 'submit',
  	'#value' => t('Confirm Settings'),
  	'#submit' => array('cmsns_page_privacy_submit'),
  );
  return $form;
}

function cmsns_page_privacy_submit($form, & $form_state) {
  global $user;
  $v = $form_state['values'];
  if($v['v']){
    db_query("UPDATE {x_qianbao_sum} SET xiaoxi = %d, haoyou = %d, qianbao = %d WHERE uid = %d", $v['x'], $v['h'], $v['q'], $user->uid);
    $t = true;
  }else{
    db_query("INSERT INTO {x_qianbao_sum} (uid, sum, qianbao, xiaoxi, haoyou) VALUES ( %d, %d, %d, %d, %d)",$user->uid, $v['q'], $v['x'], $v['h']);
    $t = true;
  }
  if($t){
    $msg = t('Private Options updated successfully');
  }else{
    $msg = t('Sorry, unknown error, failed to update');
  }
  drupal_set_message($msg);
}

function cmsns_haoyou_page_view($ac){
  global $user;
  $v = '';
  if(arg(3) == 'action'){
    if(arg(4) != 'g' && is_numeric(arg(5))){
      $b = db_fetch_object(db_query('SELECT * FROM {x_haoyou_to} WHERE uid = %d AND touid = %d', $user->uid, arg(5)));
      if($b->uid){
        $to = _cmsns_qianbao_user(array('uid' => $b->touid));
        $value = array('uid' => $user->uid, 'touid' => $b->touid, 'name' => $to->name);
      }
    }
    switch (arg(4)){
      case 'bei':
        $c = drupal_get_form('cmsns_haoyou_page_action_bei', $value);
      break;
      case 'del':
        if(!$_GET['del']){
          $c = t('Confirm to delete friend: !n', array('!n' => $to->name)). l(t('Confirm'), $_GET['q'], array('query' => array('del' => 'ok'), 'attributes' => array('class' => 'haoyou_del_ok')));
        }else{
          cmsns_haoyou_info_del($value);
          drupal_goto("user/$user->uid/haoyou");
        }
      break;
      case 'move':
        if($bb = _cmsns_haoyou_user_gid()){
          $value['gid'] = $bb;
          $c = drupal_get_form('cmsns_haoyou_page_action_move', $value);
        }else{
          drupal_set_message(t('Please add a new friend list first.'));
          drupal_goto("user/$user->uid/haoyou/action/g");
        }
      break;
      case 'ban':
        if(!$_GET['ban']){
          $c = t('Confirm to delete friend !n and move to blacklist: ', array('!n' => $to->name)).l('Confirm', $_GET['q'], array('query' => array('ban' => 'ok'), 'attributes' => array('class' => 'haoyou_del_ok')));
        }else{
          cmsns_haoyou_info_ban($value);
          drupal_goto("user/$user->uid/haoyou");
        }
      break;
      case 'removeban':
        if(!$_GET['removeban']){
          $c = l(t('Confirm to remove from blacklist: '), $_GET['q'], array('query' => array('removeban' => 'ok'))). $to->name;
        }else{
          haoyou_info_removeban($value);
          drupal_goto("user/$user->uid/haoyou");
        }
      break;
      case 'g':
        $c = drupal_get_form('cmsns_haoyou_page_action_g');
      break;
      default:
        drupal_goto("user/$user->uid/haoyou");
      break;
    }
  }
  if($c){
    $cc = '<div class="haoyou_action">'.$c.'</div>';
  }
  $result = db_query('SELECT * FROM {x_haoyou} WHERE uid = 0 OR uid = %d ORDER BY id DESC',$user->uid);
  while ($a = db_fetch_object($result)){
    if($a->id > 2){
      $editname = l(t('Revised'), "user/$user->uid/haoyou/action/g/$a->id");
    }else{
      $editname = false;
    }
    $t[] = array('data' => '<div class="haoyou_gid_title"><span class="haoyou_gid_name">'.$a->name . '</span>' . $editname. '</div>'. _cmsns_haoyou_page_view_sql($user->uid, $a->id));
  }
  $v = '<table id="haoyou_view"><tr valign="top"><td width=200>'.theme('item_list', $t, l(t('Add a new group'), "user/$user->uid/haoyou/action/g", array('attributes' => array('class' => 'haoyouiframe'))), 'ul', array('class' => 'haoyou_list')).'</td>';
  $v .= '<td>'. $cc .drupal_get_form('cmsns_haoyou_add_form') .'</td></tr></table>';
  drupal_set_message($msg);
  return $v;
}

function _cmsns_haoyou_page_view_sql($uid, $id){
  global $user;
  $result = db_query('SELECT h.msg, h.gid, u.name, u.access, u.uid FROM {x_haoyou_to} h INNER JOIN {users} u ON h.touid = u.uid WHERE h.uid = %d AND h.gid = %d ORDER BY h.gid DESC, u.access DESC', $uid, $id);
  while ($a = db_fetch_object($result)){
    $b = _cmsns_username_info($a, $a, 1);
    $t[] = $b;
  }
  return theme('item_list', $t);
}

function cmsns_haoyou_page_action_bei(&$form_state, $value){
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Memo on a friend !n', array('!n' => $value['name'])),
    '#size' => 30,
    '#description' => t('Max Length Limited to 60 characters'),
    '#prefix' => '<div id="haoyou_page_action_name">',
    '#suffix' => '</div>',
    );
  $form['submit'] = array(
  	'#type' => 'submit',
  	'#value' => t('Confirm'),
    '#submit' => array('haoyou_page_action_submit'),
    '#prefix' => '<div id="haoyou_page_action_submit">',
    '#suffix' => '</div>',
  );
  $form['value'] = array(
  	'#type' => 'value',
  	'#value' => $value,
  );
  $form['#validate'][] = 'haoyou_page_action_bei_validate';
  return $form;
}

function cmsns_haoyou_page_action_bei_validate($form, & $form_state) {
  if(drupal_strlen($form_state['values']['name']) > 60){
    form_set_error('name', t('Too long. Please enter again'));
  }else{
    $v = $form_state['values']['value'];
    if(db_query("UPDATE {x_haoyou_to} SET msg = '%s' WHERE touid = %d AND uid = %d", $form_state['values']['name'], $v['touid'], $v['uid'])){
      drupal_set_message(t('Operated Successfully'));
    }
  }
}

function cmsns_haoyou_page_action_g(&$form_state){
  global $user;
  if(is_numeric(arg(5))){
    $name = db_result(db_query('SELECT name FROM {x_haoyou} WHERE id = %d AND uid = %d', arg(5), $user->uid));
    if(!$name){
      drupal_set_message(t('Illegal access'));
      drupal_goto("user/$user->uid/haoyou");
    }
    $title = t('Change group name');
  }else{
    $title = t('Enter group name');
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => $title,
    '#default_value' => $name ? $name: null,
    '#description' => t('Max Length is 24 letters'),
    '#size' => 10,
    );
  $form['submit'] = array(
  	'#type' => 'submit',
    '#submit' => array('cmsns_haoyou_page_action_submit'),
  	'#value' => t('Confirm'),
  );
  $form['#validate'][] = 'cmsns_haoyou_page_action_g_validate';
  return $form;
}

function cmsns_haoyou_page_action_g_validate($form, & $form_state) {
  global $user;
  if(drupal_strlen($form_state['values']['name']) > 24){
    form_set_error('name', t('Group name is too long. Please re-enter it.'));
  }elseif(!is_numeric(arg(5))){
    if(db_query("INSERT INTO {x_haoyou} (uid, name) VALUES (%d, '%s')",$user->uid, $form_state['values']['name'])){
      drupal_set_message(t('New Group Added'));
    }
  }else{
   if(db_query("UPDATE {x_haoyou} SET name = '%s' WHERE id = %d", $form_state['values']['name'], arg(5))){
      drupal_set_message(t('Group name changed sucessfully'));
    }
  }
}

function cmsns_haoyou_page_action_move(&$form_state, $value){
	$form['name'] = array(
		'#title' => t('Move your friend !n to', array('!n' => $value['name'])),
		'#type' => 'select',
		'#options' => $value['gid'],
		'#default_value' => $_GET['gid'],
	);
  $form['value'] = array(
  	'#type' => 'value',
  	'#value' => $value,
  );
  $form['submit'] = array(
  	'#type' => 'submit',
    '#submit' => array('cmsns_haoyou_page_action_submit'),
  	'#value' => t('Confirm'),
  );
  $form['#validate'][] = 'cmsns_haoyou_page_action_move_validate';
  return $form;
}
function cmsns_haoyou_page_action_submit(&$form_state, $value){
  global $user;
  drupal_goto("user/$user->uid/haoyou");
}
function cmsns_haoyou_page_action_move_validate($form, & $form_state) {
  $v = $form_state['values']['value'];
  $gid = $form_state['values']['name'];
  if($_GET['gid'] == $gid){
    return drupal_set_message(t('No change'));
  }
  if(db_query('UPDATE {x_haoyou_to} SET gid = %d WHERE touid = %d AND uid = %d', $gid, $v['touid'], $v['uid'])){
    drupal_set_message(t('Operated Successfully'));
  }else{
    drupal_set_message(t('Failed to Operate'));
  }
}

function cmsns_haoyou_page_info($ac){
  global $user;
  switch(arg(4)){
    case '':
      $result = pager_query('SELECT * FROM {x_haoyou_msg} WHERE type = 1 AND (uid = %d OR touid = %d) ORDER BY id DESC', 50, 0, NULL, $user->uid, $user->uid);
    break;
    case 'set':
      $result = pager_query('SELECT * FROM {x_haoyou_msg} WHERE  type = 1 AND uid = %d ORDER BY id DESC', 50, 0, NULL, $user->uid);
    break;
    case 'get':
      $result = pager_query('SELECT * FROM {x_haoyou_msg} WHERE type = 1 AND touid = %d ORDER BY sys ASC, id DESC', 50, 0, NULL, $user->uid);
    break;
  }
  $true = false;
  while ($a = db_fetch_object($result)){
    $row = array();
    $row[] = format_date($a->time,'small');
    if($a->uid == $user->uid){
      $touser = _cmsns_qianbao_user(array('uid' => $a->touid));
      $row[] = t('You are applying to be friend for !n. P.S.: !msg', array('!n' => theme('username',$touser), '!msg' => $a->msg));
      switch ($a->sys){
        case '0':
          $row[] = t('Waiting for !n agree', array('!n' => theme('username',$touser)));
        break;
        case '1':
          $row[] = t('Already approved');
        break;
        case '2':
          $row[] = t('!n Dennied', array('!n' => theme('username',$touser)));
        break;
        case '3':
          $row[] = t('你被 !n 列为黑名单', array('!n' => $touser->name));
        break;
      }
    }else{
      $touser = _cmsns_qianbao_user(array('uid' => $a->uid));
      $row[] = t('!n want to be your friend, and his/er message: !msg', array('!n' => theme('username',$touser), '!msg' => $a->msg));
      switch ($a->sys){
        case '0':
          $row[] = l(t('Allowed'), "user/$user->uid/haoyou/info/op/ok/$touser->uid/$a->id", array('query' => drupal_get_destination())).' | '. l(t('Denied'),"user/$user->uid/haoyou/info/op/no/$touser->uid/$a->id", array('query' => drupal_get_destination())).' | '.l(t('Permanent denied'),"user/$user->uid/haoyou/info/op/kill/$touser->uid/$a->id", array('query' => drupal_get_destination()));
        break;
        case '1':
          $row[] = t('Already approved');
        break;
        case '2':
          $row[] = t('Already Dennied');
        break;
        case '3':
          $row[] = t('!n are classified as black', array('!n' => theme('username',$touser)));
        break;
      }
    }
    $rows[] = $row;
    $true = true;
  }
  $v = '<h2 class="x-a-c">'.t('Filter settings: '). l(t('All'), "user/$user->uid/haoyou/info").' | '.l(t('Send requests'), "user/$user->uid/haoyou/info/set").' | '.l(t('Received request'), "user/$user->uid/haoyou/info/get").'</h2>';
  $v .= theme('table', array(t('Time'), t('Subject'), t('Status')), $rows);
  return $v;
}

function cmsns_haoyou_page_info_op($ac){
  global $user;
  $result = db_query('SELECT * FROM {x_haoyou_msg} WHERE id = %d', arg(7));
  $v = db_fetch_object($result);
  if($v->touid == $user->uid && $v->sys == 0 && $v->type == 1){
    switch (arg(5)){
      case 'ok':
        cmsns_haoyou_info_ok(array('touid' => arg(6), 'msgid' => arg(7)));
        return drupal_goto();
      break;
      case 'no':
        db_query('UPDATE {x_haoyou_msg} SET sys = 2, uptime = %d WHERE id = %d', time(), arg(7));
        return drupal_goto();
      break;
      case 'kill':
        if(db_query("INSERT INTO {x_haoyou_to} (uid, touid, gid) VALUES (%d, %d, 1)", $user->uid, arg(6))){
          db_query('UPDATE {x_haoyou_msg} SET sys = 3, uptime = %d WHERE id = %d', time(), arg(7));
          return drupal_goto();
        }
      break;
    }
  }else{
    drupal_not_found();
  }
}

function cmsns_qianbao_page_view($ac){
  $u = _cmsns_qianbao_view($ac->uid);
  $v = '<div class="qianbaoview">'.t('Your current amount is !sum', array('!sum' => $u->sum ? $u->sum: 0)).'</div>';
  $v .= drupal_get_form('cmsns_qianbao_to_form', $u);
  return $v;
}

function cmsns_qianbao_to_form(& $form_state, $u){
	$form['value'] = array(
		'#type'=> 'textfield',
		'#title' => t('Amount to send'),
    '#required' => TRUE,
		'#size' => '10',
	);
	$form['touser'] = array(
    '#type'=> 'textfield',
    '#title' => t('Send to'),
    '#size' => 30,
    '#maxlength' => 60,
    '#required' => TRUE,
    '#default_value' => $_GET['user'],
    '#autocomplete_path' => 'user/autocomplete',
	);
	$form['msg'] = array(
		'#type'=> 'textfield',
		'#title' => t('Leave a message'),
    '#description' => t('You message will be show when she or he received money. 60 Letters Limited.'),
    '#maxlength' => 128,
		'#size' => '60',
	);
  $form['info'] = array(
  	'#type' => 'value',
  	'#value' => $u
  );
  $form['submit'] = array(
  	'#type' => 'submit',
  	'#value' => t('Confirm'),
  	'#submit' => array('cmsns_qianbao_to_form_submit'),
  );
  $form['#validate'][] = 'cmsns_qianbao_to_form_validate';
  return $form;
}

function cmsns_qianbao_to_form_validate($form, & $form_state) {
  global $user;
  if (is_numeric($form_state['values']['value']) && $form_state['values']['value'] > 0) {
    $u = $form_state['values']['info'];
    if($u->sum > ($form_state['values']['value']+$form_state['values']['value']*$u->bankhui)){
      $tu = _cmsns_qianbao_user(array('name' => $form_state['values']['touser']));
      if($tu->uid){
        if($tu->uid == $user->uid){
          form_set_error('touser', t('You cannot send it to yourself.'));
        }
      }else{
        form_set_error('touser', t('Receiver does not exist.'));
      }
    }else{
      form_set_error('value', t('Out of you total amount to send.'));
    }
  }else{
    form_set_error('value', t('Amount is empty and must be greater than 0.'));
  }
}

function cmsns_qianbao_to_form_submit($form, & $form_state) {
  global $user;
  $tou = _cmsns_qianbao_user(array('name' => $form_state['values']['touser']));
  $info = $form_state['values']['info'];
  if($t = cmsns_qianbao_addset(array('uid' => $user->uid, 'touid' => $tou->uid, 'value' => $form_state['values']['value'], 'type' => 'zhuanzhang', 'msg' => $form_state['values']['msg']))){
    drupal_set_message(t('The amount of $%v has been sent via %bank, and fee is %h (charged %hui %). Balance of your wallet is %upsum.', array('%bank' => $info->bankname, '%v' => $t['value'], '!to' => theme('cmsns_username',$tou), '%h' => $t['hui'], '%hui' => ($info->bankhui*100), '%upsum' => $t['updatesum'])));
  }else{
    form_set_error('errer', t('Transaction system is in maintenance.'));
  }
}

function cmsns_qianbao_page_info($ac){
  global $user;
  $t = '';
  switch (arg(4)){
    case 'add':
      $sql = '(ref = 1 AND uid = '.$user->uid.') OR touid = '.$user->uid;
    break;
    case 'remove':
      $sql = 'ref = 0 AND uid = '.$user->uid;
    break;
    default:
      $sql = 'uid = '.$user->uid.' OR touid = '.$user->uid;
    break;
  }
  $result = pager_query('SELECT uid, touid, type, time, value, msg, tosum, sum, hui, link FROM {x_qianbao_to} WHERE '.$sql.' ORDER BY time DESC', 20, 0);
  $comm = t('Additional Message:');
  while ($a = db_fetch_object($result)) {
    switch ($a->type){
      case 'zhuanzhang':
        if($a->uid == $user->uid){
          $u = _cmsns_qianbao_user(array('uid' => $a->touid));
          $t[][] = t('!t(!i ago)，你给 !n 汇去了 !v 元，手续费 !b(汇率 !h)，当前你的现金总额为：!c 元。!msg', array('!t' => format_date($a->time,'small'), '!i' => format_interval(time() - $a->time), '!n' => theme('cmsns_username',$u), '!v' => $a->value, '!b' => $a->value*$a->hui, '!h' => ($a->hui*100).'%', '!c' => $a->sum, '!msg' => ($a->msg ? $comm.$a->msg: NULL)));
        }else{
          $u = _cmsns_qianbao_user(array('uid' => $a->uid));
          $t[][] = t('!t(!i ago), !n sent you $!v, and your current balance is $!c. !msg', array('!t' => format_date($a->time,'small'), '!i' => format_interval(time() - $a->time), '!n' => theme('cmsns_username',$u), '!v' => $a->value, '!c' => $a->sum, '!msg' => ($a->msg ? $comm.$a->msg: NULL)));
        }
      break;
      case 'user':
        $t[][] = t('!t(!i ago), you earn $!v from ilink, your current balace is $!a.', array('!t' => format_date($a->time,'small'), '!i' => format_interval(time() - $a->time), '!v' => $a->value, '!a' => $a->sum, '!link' => '<a href="/'.$a->link.'">' .$a->msg. '</a>'));
      break;
      default:
        if($a->value > 0){
          $m = t(' earned ');
        }else{
          $m = t(' deducted "');
        }
        $t[][] = t('!t(!i ago), you earn $!v from ilink, your current balace is $!a.', array('!t' => format_date($a->time,'small'), '!i' => format_interval(time() - $a->time), '!v' => $a->value, '!a' => $a->sum, '!link' => '<a href="/'.$a->link.'">' .$a->msg. '</a>'.$m));
      break;
    }
  };
  drupal_set_title(t('Account Detail'));
  return theme('table', array(t('Filter settings: ') .l(t('Income Only'),"user/$user->uid/qianbao/info/add").l(t('Spend Only'),"user/$user->uid/qianbao/info/remove").l(t('View All'),"user/$user->uid/qianbao/info")), $t ,array('id' => 'qianbaoinfo')).theme('pager',20, 0);
}